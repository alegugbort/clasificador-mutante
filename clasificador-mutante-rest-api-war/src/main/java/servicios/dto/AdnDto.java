package servicios.dto;

import java.io.Serializable;


public class AdnDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String[] dna;

	public String[] getDna() {
		return dna;
	}

	public void setDna(String[] adn) {
		this.dna = adn;
	}

	public AdnDto(String[] dna) {
		this.dna = dna;
	}
	
	public AdnDto() {
	}


	

}
