package servicios.rest;


import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;

import meli.proyectomutantes.excepciones.ExcepcionADNMalFormado;
import meli.proyectomutantes.manejadores.ManejadorMutanteHumano;
import meli.proyectomutantes.pojo.Estadisticas;
import servicios.dto.AdnDto;
import servicios.dto.EstadisticasDto;



@Path("/")
@RequestScoped
public class ServiciosExpuestos {
	@Inject
	ManejadorMutanteHumano administrador;
	
	private static Logger log = Logger.getLogger(ServiciosExpuestos.class.getName());

	@POST
	@Path("/mutant")
	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	public Response esMutante(AdnDto adn) {
		Response respuesta = Response.status(Response.Status.FORBIDDEN).build();
		try {
			
			if (administrador.esMutante(adn.getDna()))
			{
				respuesta = Response.ok().build();
			}
			
		}
		catch (ExcepcionADNMalFormado e) {
//			Aqui segun los requerimientos no tengo mucho para hacer, pero la idea seria que en esta parte del codigo se controlen los errores 
//			y se devuelva lo pertinente segun el contraro. En este caso se loguea y se devuelve 403.
			log.debug(e.getMessage());
		}
		catch (Exception e) {
//			Capturo cualquier otra exepcion para no difundir algun dato que no quiera, por ejemplo versiones de servidor. 
//			El error lo dejo en el log
			log.info(e.getMessage());
			respuesta = Response.serverError().build();
		}
		return respuesta;
	}
	
	@GET
	@Path("/stats")
	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	public Response conseguirEstadisticas() {
		Response respuesta = null;
		try {
			Estadisticas estadisticas = administrador.consultarEstaditicas();
			EstadisticasDto estadisticasDto = new EstadisticasDto(estadisticas.getCantidadMutantes(), estadisticas.getCantidadHumanos(), estadisticas.getRatio());
			respuesta = Response.ok().entity(estadisticasDto).build();
		}
		catch (Exception e) {
//			Capturo cualquier otra exepcion para no difundir algun dato que no quiera, por ejemplo versiones de servidor. 
//			El error lo dejo en el log
			log.info(e.getMessage());
			respuesta = Response.serverError().build();
		}
		return respuesta;
	}


}
