package servicios.rest.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URL;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.logging.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import meli.proyectomutantes.cache.CacheEstadisticas;
import meli.proyectomutantes.colamensajes.ColaMensajes;
import meli.proyectomutantes.constantes.CantidadRepetidosDireccion;
import meli.proyectomutantes.dao.DatoMutanteHumanoDaoImpl;
import meli.proyectomutantes.excepciones.ExcepcionADNMalFormado;
import meli.proyectomutantes.manejadores.ManejadorMutanteHumanoImpl;
import meli.proyectomutantes.modelo.DatoMutanteHumano;
import meli.proyectomutantes.pojo.ADN;
import servicios.dto.AdnDto;
import servicios.dto.EstadisticasDto;
import servicios.rest.ServiciosExpuestos;

@RunWith(Arquillian.class)
public class TestServiciosRest {

	private static Logger log = Logger.getLogger(ServiciosExpuestos.class.getName());
	
	private final String metodoVerificarMutante = "mutant";
	private final String metodoEstadisticas = "stats";

	@Deployment
	public static WebArchive createDeployment() {
//		Con este codigo creo un ejecutable que tiene todos los paquetes y la persitencia

//		Importo las dependencias maven
        File[] files = Maven.resolver()
                            .loadPomFromFile("../clasificador-mutante-logica-ejb/pom.xml")
                            .importRuntimeDependencies()
                            .resolve()
                            .withTransitivity()
                            .asFile();
        
        
		
		return ShrinkWrap.create(WebArchive.class, "prueba.war")
				.addPackage(CantidadRepetidosDireccion.class.getPackage())
				.addPackage(DatoMutanteHumano.class.getPackage())
				.addPackage(ExcepcionADNMalFormado.class.getPackage())
				.addPackage(ManejadorMutanteHumanoImpl.class.getPackage())
				.addPackage(DatoMutanteHumanoDaoImpl.class.getPackage())
				.addPackage(ADN.class.getPackage())
				.addPackage(CacheEstadisticas.class.getPackage())
				.addPackage(ColaMensajes.class.getPackage())
				.addPackage(AdnDto.class.getPackage())
				.addPackage(ServiciosExpuestos.class.getPackage())
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource("test-ds.xml", "test-ds.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsLibraries(files);
	}

// considero las direcciones como sur (arriba -> abajo), 
//	este (izquierda->derecha)
//	sureste (izquierda-arriba->derecha-abajo)
//	noreste (izquierda->abajo)->derecha-arriba)

	@ArquillianResource
	URL url;

	@Test
	@InSequence(1)
	public void mutanteSurEste() {

		ResteasyClient cliente = new ResteasyClientBuilder().build();
		ResteasyWebTarget objetivo = cliente.target(url.toString() + metodoVerificarMutante);
		AdnDto adn = new AdnDto();
		String[] cadena = { "ATGCGA", "CAGTCC", "TTATCT", "AGCAGG", "CCGAAA", "TCACTG" };
		adn.setDna(cadena);
		Response response = objetivo.request().post(Entity.entity(adn, MediaType.APPLICATION_JSON));
		response.close();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

	@Test
	@InSequence(2)
	public void mutanteEste() {

		ResteasyClient cliente = new ResteasyClientBuilder().build();
		ResteasyWebTarget objetivo = cliente.target(url.toString() + metodoVerificarMutante);
		AdnDto adn = new AdnDto();
		String[] cadena = { "ATGCGA", "CTGTCC", "TTAACT", "AGCAGG", "CCAAAA", "TCACTG" };
		adn.setDna(cadena);

		Response response = objetivo.request().post(Entity.entity(adn, MediaType.APPLICATION_JSON));
		response.close();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

	@Test
	@InSequence(3)
	public void mutanteNorEste() {

		ResteasyClient cliente = new ResteasyClientBuilder().build();
		ResteasyWebTarget objetivo = cliente.target(url.toString() + metodoVerificarMutante);
		AdnDto adn = new AdnDto();
		String[] cadena = { "ATGCGA", "CTGTCC", "TTACCT", "AGCAGG", "CCACAA", "TCACTG" };
		adn.setDna(cadena);

		Response response = objetivo.request().post(Entity.entity(adn, MediaType.APPLICATION_JSON));
		response.close();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}

	@Test
	@InSequence(4)
	public void mutanteSur() {

		ResteasyClient cliente = new ResteasyClientBuilder().build();
		ResteasyWebTarget objetivo = cliente.target(url.toString() + metodoVerificarMutante);
		AdnDto adn = new AdnDto();
		String[] cadena = { "ATGCGA", "CTGTCC", "TTACCT", "ATCAGG", "CTACAA", "TCACTG" };
		adn.setDna(cadena);

		Response response = objetivo.request().post(Entity.entity(adn, MediaType.APPLICATION_JSON));
		response.close();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

	@Test
	@InSequence(5)
	public void matrizMalFormada() {

		ResteasyClient cliente = new ResteasyClientBuilder().build();
		ResteasyWebTarget objetivo = cliente.target(url.toString() + metodoVerificarMutante);
		AdnDto adn = new AdnDto();
		String[] cadena = { "ATGCGA", "CTGTCCAA", "TTACCT", "AGCAGG", "CCACAA" };
		adn.setDna(cadena);

		Response response = objetivo.request().post(Entity.entity(adn, MediaType.APPLICATION_JSON));
		response.close();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());

	}

	@Test
	@InSequence(6)
	public void noMutante() {

		ResteasyClient cliente = new ResteasyClientBuilder().build();
		ResteasyWebTarget objetivo = cliente.target(url.toString() + metodoVerificarMutante);
		AdnDto adn = new AdnDto();
		String[] cadena = { "ATGCGA", "CAGTCC", "TTACCT", "AGAGGG", "CCGAAA", "TCACTG" };
		adn.setDna(cadena);

		Response response = objetivo.request().post(Entity.entity(adn, MediaType.APPLICATION_JSON));
		response.close();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());

	}
	
	@Test
	@InSequence(7)
	public void noMutanteRepetido() {

		ResteasyClient cliente = new ResteasyClientBuilder().build();
		ResteasyWebTarget objetivo = cliente.target(url.toString() + metodoVerificarMutante);
		AdnDto adn = new AdnDto();
		String[] cadena = { "ATGCGA", "CAGTCC", "TTACCT", "AGAGGG", "CCGAAA", "TCACTG" };
		adn.setDna(cadena);

		Response response = objetivo.request().post(Entity.entity(adn, MediaType.APPLICATION_JSON));
		response.close();

		assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());

	}
	

	
	

	@Test
	@InSequence(8)
	public void consultaEstadisticas() {

		ResteasyClient cliente = new ResteasyClientBuilder().build();
		ResteasyWebTarget objetivo = cliente.target(url.toString() + metodoEstadisticas);
		
		Response response = objetivo.request().get();
		EstadisticasDto estadisticas = (EstadisticasDto) response.readEntity(EstadisticasDto.class);
		response.close();

		
		log.info("*****Estadisticas:*****");
		log.info("Cantidad mutantes:" + String.valueOf(estadisticas.getCount_mutant_dna()));
		log.info("Cantidad humanos:" + String.valueOf(estadisticas.getCount_human_dna()));
		log.info("Cantidad ratio:" + String.valueOf(estadisticas.getRatio()));
		log.info("*****Fin Estadisticas:*****");
		
		assertEquals(Double.valueOf(((double)estadisticas.getCount_mutant_dna())/(estadisticas.getCount_human_dna()+estadisticas.getCount_mutant_dna())), Double.valueOf(estadisticas.getRatio()));
	
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	}

}
