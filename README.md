Proyecto prueba meli mutantes:

La solucion conciste en recorrer la matriz de derecha a izquierda y de arriba hacia abajo. A medida que se recorre, se genera una estructura con la cantidad de elementos que se tienen en cada direccion con la base nitrogenada de la posicion en cuestion. De esa forma si al recorrer una posicion ya pase por la misma verifico el dato previamente guardado.

La solucion es de orden n cuadrado en peor caso, con n la cantidad de elementos que tiene la matriz. Para soportar un flujo alto de trafico la persistencia de cada consulta de adn la hago en forma asincrona. 

La aplicacion esta compuesta por un ear, y dentro un war como capa de servicios y un ejb empaquetado en un jar que resuleve la logica de negocio. Hay una Fachada para la logica de negocio. Tambien se separa el acceso a los datos utilizando el patron dao. Notar que la aplicacion es pura Java EE, no cuenta con ninguna dependencia dentro del empaquetado.


Por otro lado la arquitectura de la solucion escala de forma horizontal y el esquema de deploy es asi:

                                                        |https rest|
                                                    --------------
                                                    |   Apache    |
                                                    | Balanceador |
                                                    --------------
                                                         /  \
                                                        /    \
                                           |http rest| /      \  |http rest|
                                            --------------    --------------
                                            |   wildfly   |  |   wildfly   |
                                            |      1      |  |      2      |
                                            --------------    --------------
                                                    \           /
                                                     \         /
                                                        |jdbc| 
                                                    --------------   
                                                    |     db2     |  
                                                    |     base    | 
                                                    --------------  

La solucion tiene el despligue integrado con gitlab. Esta todo el deploy automatizado, utilizando variables de entorno definidas en el pipeline. Si se quiere generar el codigo y correr las pruebas automatizadas hay que ejecutar el comando:

                                        *** mvn clean package -Pjacoco ****

En la carpeta target queda el ear generado y el cubrimiento del codigo se puede ver en en la carpeta site dentro target. Notar que esta configurado para que en caso de que no se cubra el 80% no genere el ejecutable. Esto esta automatizado en el repositorio como parte de la integracion/deliveracion continua. Ademas el pipeline esta configurado para dejar los resultados del cubrimiento del codigo expuesto a internet separando el resultado del cubrimiento del codigo con /war-coverage para el war y /ejb-coverage para el codigo del ejb.

Luego en caso de querer ejecutar todo el enterno se debe tener instalado docker-compose. Se separaron los distintos dockers para elegir si se quiere armar todo el ambiente o parte del mismo. Como se ve en el .gitlab-ci, en la nube se ejecutan todos.

                                           1- CREAR BASE DE DATOS.

Si se quiere utilizar la base db2 que tiene la solucion primero debe de setear las variables de entorno en el script bd.sh:

+usuarioBD: con el usuario de la base de datos.
+passDB: con la contraseña de la base de datos.
+nombreBD: nombre de la base de datos.
+schemaBD: nombre del esquema.
+persistenciaBD: la ruta a donde se encuentra le persistencia de los datos. Esto evita que al bajar el docker se pierdan los datos. Es importante que la ruta exista y sea absoluta o relativa a la raiz de la carpeta db2.

El script luego ejecuta el siguiente comando:

docker-compose -f db2/docker-compose-base-datos.yml up -d --no-recreate

Con docker ps se puede ver los contenedores que estan corriendo. De ahi se puede sacar el contenedor-id
se puede ver el log con docker logs -f contenedor-id para verificar que la base de datos subio y se creo correctamente, la primera vez demora unos segundos. Ademas este comando se ejecuta con --no-recreate para evitar crear el motor de bd cada vez. 

                                            
 
                                          2- EJECUTAR LA APLICACION.

Para levantar el ambiente se debe de setear las siguientes variables en el script wildfly.sh. Notar que si se desea utilizar la base db2 del script anterior los datos deben de coincidir.

+host: ip privada del servidor.
+usuarioBD: con el usuario de la base de datos.
+passDB: con la contraseña de la base de datos.
+rutaDB: ruta tal cual va en el servidor de aplicaciones. Si se utiliza la base de datos anterior la ruta se compone por jdbc:db2://${host}:50000/${nombreBD}:currentSchema=${schemaBD};

El script luego copia el ejecutable y los resultados de los tests en la carpeta wildfly. 

El script luego ejecuta el siguiente comando: 

docker-compose -f wildfly/docker-compose-wildfly.yml up -d --build

                                            3- CREAR SERVIDOR APACHE.

Para crear el servidor apache se necesita tener el puerto 80 expuesto a internet para poder crear el certificado https con LetsEncrypt. Se deben de setear las variables en el script apache.sh:

+host: ip privada del servidor.
+dominio: nombre de dominio con el que se expone a internet.
+mail: el correo con el que se crea el certificado.
+rutaEtcLetsEncrypt: ruta a un directorio donde se van a guardar los certificados. Es importante que la ruta exista y sea absoluta o relativa a la raiz de la carpeta apache.
+rutaVarLetsEncrypt: ruta a un directorio donde se van a guardar los informacion de LetsEncript. Es importante que la ruta exista y sea absoluta o relativa a la raiz de la carpeta apache.

El script luego ejecuta el siguiente comando:
docker-compose -f apache/docker-compose-apache.yml up -d --build

Por ultimo los servicios estan: 

                    https://meliprueba.ddns.net/meli/stats + para las estadisticas.
            https://meliprueba.ddns.net/meli/mutant + para las el servicio que clasifica los mutantes.
            https://meliprueba.ddns.net/ejb-coverage + para ver el cubrimiento del codigo del ejb.
                https://meliprueba.ddns.net/war-coverage + para ver el cubrimiento del codigo del war.
                
******************************************************************************************************************************************

Estuve pensando un poco mas en la solucion y vi que el flujo alto de trafico no lo iba a resolver bien solo con un metodo asincrono. Por tal motivo configure una cola la cual esta limita por defecto con un maximo de 64 threads concurrentes. En esta direccion la bd tiene la misma cantidad de conexiones. Ambos numeros deberian aumentarse para soportar rafagas tan grandes. Ademas configure una cache hazelcast para la consulta del ratio. La misma dura 1 segundo (Configurable). Estas caches se pueden clusterizar y trabajar en conjunto con los distintos nodos. Disculpen por los cambios pero me quedo girando en la cabeza. El ear tiene las librerias de la cache que salen del estandar jee.
