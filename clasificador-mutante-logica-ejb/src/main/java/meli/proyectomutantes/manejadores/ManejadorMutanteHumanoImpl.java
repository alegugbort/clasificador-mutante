package meli.proyectomutantes.manejadores;



import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;

import org.jboss.logging.Logger;

import meli.proyectomutantes.cache.CacheEstadisticas;
import meli.proyectomutantes.constantes.BaseNitrogenedaEnum;
import meli.proyectomutantes.constantes.CantidadRepetidosDireccion;
import meli.proyectomutantes.constantes.DireccionesEnum;
import meli.proyectomutantes.dao.DatoMutanteHumanoDao;
import meli.proyectomutantes.excepciones.ExcepcionADNMalFormado;
import meli.proyectomutantes.modelo.DatoMutanteHumano;
import meli.proyectomutantes.pojo.ADN;
import meli.proyectomutantes.pojo.BaseNitrogenadaInfo;
import meli.proyectomutantes.pojo.Estadisticas;

@Stateless
public class ManejadorMutanteHumanoImpl implements ManejadorMutanteHumano {

	
	private static Logger log = Logger.getLogger(ManejadorMutanteHumanoImpl.class.getName());

//	@Inject
//	DatoMutanteHumanoDao datoMutanteHumanoDao;
	
	@Inject
	CacheEstadisticas cache;
	
	@Inject
	private Queue colaBaseDatos; 
	
	@Inject
    private JMSContext context;
	
	@Override
	public boolean esMutante(String[] adn) throws ExcepcionADNMalFormado {
		
		ADN estructuraAdn = new ADN();
		int cantidadBuscada = CantidadRepetidosDireccion.cuatro;
		estructuraAdn.setTamaMatriz(adn.length);

		for (int y = 0; y < adn.length; y++) {
			for (int x = 0; x < adn[y].length(); x++) {
				for (DireccionesEnum dir : DireccionesEnum.values()) {
					int cantidadEnEstaDir = calcularRelacionesUnaBaseNitrogenada(y, x,
							BaseNitrogenedaEnum.valueOf(String.valueOf(adn[y].charAt(x))), estructuraAdn, adn, dir,
							cantidadBuscada);

//					Compruebo que la matriz esta bien formada. De no estar bien tiro una exepcion. Puede ser discutible si es necesario validar estos parametros.
					estructuraAdn.matrizCorrecta(y, x);
					
					log.debug("Encontre una cadena de:" + String.valueOf(cantidadEnEstaDir));
					log.debug("posicion actual y: " + String.valueOf(y) + ", x: " + String.valueOf(x)+ " Direccion encontrada:" + String.valueOf(dir.getDireccion()));

					if (cantidadEnEstaDir >= cantidadBuscada ) {
//						 Corto la ejecucion del metodo porque encontre un camino que repite la misma
//						 base nitrogenada la cantidad de veces necesarias.

						log.debug("Encontre una cadena de la cantidad buscada (" + String.valueOf(cantidadBuscada) + ")");
						
//						Salvo el adn de mutante, lo voy a hacer de forma asincrona para que el cliente no quede esperando y
//						lo hago con una cola para no perder los datos a guardar y limitar las conexiones contra la base de datos.
						DatoMutanteHumano dato = new DatoMutanteHumano();
						dato.setAdn(String.join("-", adn));
						dato.setEsMutante(Boolean.valueOf(true));
						context.createProducer().send(colaBaseDatos, dato);
						
						return true;

					}
				}
			}
		}

//		Salvo el adn de un humano, lo voy a hacer de forma asincrona para que el cliente no quede esperando y 
//		lo hago con una cola para no perder los datos a guardar y limitar las conexiones contra la base de datos.
		
		DatoMutanteHumano dato = new DatoMutanteHumano();
		dato.setAdn(String.join("-", adn));
		dato.setEsMutante(Boolean.valueOf(false));
		context.createProducer().send(colaBaseDatos, dato);
		
		return false;
	}
//	Con este metodo busco recursivamente si existe un camino valido con la basenitrogenada buscada
	public int calcularRelacionesUnaBaseNitrogenada(int y, int x, BaseNitrogenedaEnum baseBuscada, ADN estructuraADN,
			String[] AdnParam, DireccionesEnum direccion, int cantidadBuscada) {

		
//		 si los parametros en este llamado recursivo no dan una posicion valida en la
//		 matriz o el elemento que busco es distinto en esta posicion entonces retorno cero.
//		 Se corta la cadena
		if (!estructuraADN.paramatrosRecorridaValidos(y, x) || baseBuscada.getBaseNitrogeneda() != AdnParam[y].charAt(x))
			return CantidadRepetidosDireccion.ninguno;

		BaseNitrogenadaInfo baseNitrogenadaInfo = estructuraADN.getBaseNitrogenada(y, x);
//		Nunca pase por esta posicion en la matriz pero el elemento que busco coincide, voy a buscar el adyacente en la misma direccion
		if (baseNitrogenadaInfo == null) {
			baseNitrogenadaInfo = new BaseNitrogenadaInfo();
			baseNitrogenadaInfo.setBaseNitrogenadaActual(baseBuscada);
			estructuraADN.setBaseNitrogenada(y, x, baseNitrogenadaInfo);
		} else {
//			Ya pase por esta posicion en la matriz y el elemento que busco coincide, voy a buscar el adyacente en la misma direccion
//			Si ya busque en esa direccion uso el valor ya calculado
			Integer cantidadRepeticionesDireccionActual = baseNitrogenadaInfo.getMapCantDireccionesRepetidos(direccion);
			if (cantidadRepeticionesDireccionActual != null) {
				return cantidadRepeticionesDireccionActual.intValue() + 1;
			}

		}

		int cantidadEnEstaObtenidasEnEstaDireccion = CantidadRepetidosDireccion.sinInicializar;
		switch (direccion) {
		case ESTE:
			cantidadEnEstaObtenidasEnEstaDireccion = calcularRelacionesUnaBaseNitrogenada(y, x + 1, baseBuscada,
					estructuraADN, AdnParam, direccion, cantidadBuscada);
			break;
		case SUR:
			cantidadEnEstaObtenidasEnEstaDireccion = calcularRelacionesUnaBaseNitrogenada(y + 1, x, baseBuscada,
					estructuraADN, AdnParam, direccion,  cantidadBuscada);
			break;
		case NORESTE:
			cantidadEnEstaObtenidasEnEstaDireccion = calcularRelacionesUnaBaseNitrogenada(y - 1, x + 1, baseBuscada,
					estructuraADN, AdnParam, direccion,  cantidadBuscada);
			break;
		case SURESTE:
			cantidadEnEstaObtenidasEnEstaDireccion = calcularRelacionesUnaBaseNitrogenada(y + 1, x + 1, baseBuscada,
					estructuraADN, AdnParam, direccion, cantidadBuscada);
			break;
		}
//		En este punto se que me el elemento actual me sirve y suma uno al camino buscado.  
		baseNitrogenadaInfo.setMapCantDireccionesRepetidos(direccion,
				Integer.valueOf(cantidadEnEstaObtenidasEnEstaDireccion));
		return cantidadEnEstaObtenidasEnEstaDireccion + 1;
	}
	
	
	@Override
	public Estadisticas consultarEstaditicas() {
		//Tengo una cache que dura un segundo por si este metodo se consulta muchas veces tambien, me cubro para no ir siempre contra la base.
		return cache.consultarEstadisticas();
	}

}
