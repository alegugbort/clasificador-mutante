package meli.proyectomutantes.manejadores;

import javax.ejb.Local;

import meli.proyectomutantes.excepciones.ExcepcionADNMalFormado;
import meli.proyectomutantes.pojo.Estadisticas;

@Local
public interface ManejadorMutanteHumano {

	public boolean esMutante(String[] adn) throws ExcepcionADNMalFormado;
	
	public Estadisticas consultarEstaditicas();
}
