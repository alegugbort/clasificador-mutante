package meli.proyectomutantes.colamensajes;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

import org.jboss.logging.Logger;

import meli.proyectomutantes.dao.DatoMutanteHumanoDao;
import meli.proyectomutantes.modelo.DatoMutanteHumano;

@MessageDriven(name = "MDBGuardarBaseDatos", activationConfig = {
	        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "queue/GuardarBaseDatosQueue"),
	        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
	        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")})
	public class ColaMensajes implements MessageListener {

	    private static final Logger LOGGER = Logger.getLogger(ColaMensajes.class.toString());
	    
	    @Inject
	    DatoMutanteHumanoDao dao;

	    public void onMessage(Message mensajeRecibido) {
	        
	        try {
	        	ObjectMessage mensajeRecibidoObject = (ObjectMessage) mensajeRecibido;
//	            if (rcvMessage instanceof TextMessage) {
//	                msg = (TextMessage) rcvMessage;
	                LOGGER.info("Recibi mensaje: " + ((DatoMutanteHumano)(mensajeRecibidoObject.getBody(DatoMutanteHumano.class))).toString());
	                dao.crearDatoMutanteHumano(mensajeRecibidoObject.getBody(DatoMutanteHumano.class));
	        } catch (JMSException e) {
	            throw new RuntimeException(e);
	        }
	    }
}
