package meli.proyectomutantes.colamensajes;




import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;
import javax.jms.Queue;

@JMSDestinationDefinitions(
        value = {
            @JMSDestinationDefinition(
                    name = "java:/queue/GuardarBaseDatosQueue",
                    interfaceName = "javax.jms.Queue",
                    destinationName = "MDBGuardarBaseDatos"
            )
        }
)
@Startup
@Singleton
public class RecursosColaMensajes {
     
    @Resource(lookup = "java:/queue/GuardarBaseDatosQueue")
    private Queue colaBaseDatos;      
    
    @Produces
    public Queue colaBaseDatos() {
        return this.colaBaseDatos;
    }
}

