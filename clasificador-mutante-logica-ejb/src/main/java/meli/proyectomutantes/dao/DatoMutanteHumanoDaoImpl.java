package meli.proyectomutantes.dao;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import meli.proyectomutantes.modelo.DatoMutanteHumano;
import meli.proyectomutantes.pojo.Estadisticas;

@Stateless
public class DatoMutanteHumanoDaoImpl implements DatoMutanteHumanoDao{

	@Inject
    private EntityManager em;
	

	
	private final String consultaEstadisticas = "select NEW meli.proyectomutantes.pojo.Estadisticas("
			+ "sum(case when t.esMutante=true then 1 else 0 end),"
			+ "sum(case when t.esMutante=false then 1 else 0 end),"
			+ "sum(case when t.esMutante=true then 1 else 0 end)*1.0/count(t))"
			+ "from DatoMutanteHumano t"; 
	
	@Override
//	@Asynchronous
	public void crearDatoMutanteHumano(DatoMutanteHumano datoMutante) {
//		Este metodo es atomico
		em.persist(datoMutante);

	}
	
	@Override
	public Estadisticas consultarEstadisticas() {
		
		Estadisticas consulta = em.createQuery(consultaEstadisticas, Estadisticas.class).getSingleResult();
		return consulta;
		
		
	}

}
