package meli.proyectomutantes.dao;

import javax.ejb.Local;

import meli.proyectomutantes.modelo.DatoMutanteHumano;
import meli.proyectomutantes.pojo.Estadisticas;

@Local
public interface DatoMutanteHumanoDao {

	public void crearDatoMutanteHumano(DatoMutanteHumano datoMutante);
	
	public Estadisticas consultarEstadisticas();

}
