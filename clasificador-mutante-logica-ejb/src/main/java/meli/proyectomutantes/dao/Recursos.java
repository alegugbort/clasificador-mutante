package meli.proyectomutantes.dao;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
public class Recursos{

  @PersistenceContext()
  private EntityManager entityManager;

  @Produces
  public EntityManager entityManager(){
    return entityManager;
  }

}
