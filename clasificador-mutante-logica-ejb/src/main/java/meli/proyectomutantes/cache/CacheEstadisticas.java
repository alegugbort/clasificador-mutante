package meli.proyectomutantes.cache;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.annotation.CacheKey;
import javax.cache.annotation.CacheResult;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.AccessedExpiryPolicy;
import javax.cache.expiry.Duration;
import javax.cache.spi.CachingProvider;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import meli.proyectomutantes.dao.DatoMutanteHumanoDao;
import meli.proyectomutantes.pojo.Estadisticas;

@Startup
@Singleton
public class CacheEstadisticas {

	private Cache<Integer, Object> cacheEstadisticas;

	@Inject
	DatoMutanteHumanoDao dao;

	private static final Logger log = Logger.getLogger(CacheEstadisticas.class.toString());

	final static long tiempoDuracionCacheEnSegundos = 10;

	final static String nombreCache = "Estadisticas";
	
	final static Integer clave = Integer.valueOf(1);

	public synchronized Estadisticas actualizarCache()
	{
		log.info("Entre a actualizarCache");
		Estadisticas estadistica = (Estadisticas) cacheEstadisticas.get(clave);	
//		Verifico si las estadisticas no fueron cargadas mientras estuve bloqueado, sino la cargo.
		if(estadistica==null)
		{
			log.info("Estadistica null, voy a la base de datos");
			estadistica= dao.consultarEstadisticas();
			cacheEstadisticas.put(clave, estadistica);
		}
		return estadistica;
		
	}
	
	
	public Estadisticas  consultarEstadisticas() {
		log.info("Entre a consultar estadisticas");
		Estadisticas estadistica = (Estadisticas) cacheEstadisticas.get(clave);
		if(estadistica==null)
		{
//			Este metodo no lo hago sincronico para que todos los request que puedan ser alimentados por la cache al mismo tiempo, lo sean
			estadistica=actualizarCache();
		}
		return estadistica;
	}

	@PostConstruct
	public void init() {
		CachingProvider proveedorCache = Caching.getCachingProvider();
		CacheManager manejadorCache = proveedorCache.getCacheManager();
		MutableConfiguration<Integer, Object> configuracion = new MutableConfiguration<Integer, Object>();
		configuracion.setTypes(Integer.class, Object.class);
		configuracion.setStoreByValue(false);
		configuracion.setStatisticsEnabled(true);
		configuracion.setManagementEnabled(true);
		configuracion.setExpiryPolicyFactory(
				AccessedExpiryPolicy.factoryOf(new Duration(TimeUnit.SECONDS, tiempoDuracionCacheEnSegundos)));
		cacheEstadisticas = manejadorCache.createCache(nombreCache, configuracion);
	}
}
