package meli.proyectomutantes.excepciones;

public class ExcepcionADNMalFormado extends Exception {
 	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExcepcionADNMalFormado(String mensaje) {
		super(mensaje);
		
	}
	

}
