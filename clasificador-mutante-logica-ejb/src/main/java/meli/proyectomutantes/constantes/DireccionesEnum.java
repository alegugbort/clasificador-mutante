package meli.proyectomutantes.constantes;

public enum DireccionesEnum {
	ESTE('1'), SUR('2'), SURESTE('3'), NORESTE('4');

	private final char direccion;

	DireccionesEnum(final char direccion) {
		this.direccion = direccion;
	}

	public char getDireccion() {
		return direccion;
	}


}
