package meli.proyectomutantes.constantes;

public enum BaseNitrogenedaEnum {
	A('A'), T('T'), C('C'), G('G'), Ninguno(' ');

	private final char baseNitrogeneda;

	BaseNitrogenedaEnum(final char baseNitrogeneda) {
		this.baseNitrogeneda = baseNitrogeneda;
	}

	public char getBaseNitrogeneda() {
		return baseNitrogeneda;
	}


}
