package meli.proyectomutantes.pojo;

import meli.proyectomutantes.constantes.MensajesError;
import meli.proyectomutantes.excepciones.ExcepcionADNMalFormado;

public class ADN {
	


	int tamaMatriz = 0;

	public int getTamaMatriz() {
		return tamaMatriz;
	}

	public void setTamaMatriz(int tamaMatriz) {
		this.tamaMatriz = tamaMatriz;
	}

	private BaseNitrogenadaInfo[][] ADN = null;

	public boolean paramatrosRecorridaValidos(int y, int x)
	// Este metodo valida si la posicion cae dentro de la matriz
	{
		return (tamaMatriz > 0 && x < tamaMatriz && y < tamaMatriz && y >= 0 && x >= 0);
	}

	public BaseNitrogenadaInfo getBaseNitrogenada(int y, int x) {
		BaseNitrogenadaInfo info = null;

		if (ADN != null) {
			info = ADN[y][x];
		}
		return info;
	}

	public void setBaseNitrogenada(int y, int x, BaseNitrogenadaInfo baseNitrogenadaInfo) {

		if (ADN == null) {
			ADN = new BaseNitrogenadaInfo[tamaMatriz][tamaMatriz];
		}

		ADN[y][x] = baseNitrogenadaInfo;
	}
	
	public void matrizCorrecta(int y, int x) throws ExcepcionADNMalFormado{
		if (!paramatrosRecorridaValidos(y,x))
		{
			throw new ExcepcionADNMalFormado(MensajesError.errorParametros);
		}
	}


}
