package meli.proyectomutantes.pojo;

import java.util.EnumMap;
import java.util.Map;

import meli.proyectomutantes.constantes.BaseNitrogenedaEnum;
import meli.proyectomutantes.constantes.DireccionesEnum;

public class BaseNitrogenadaInfo {
	
	
	BaseNitrogenedaEnum baseNitrogenadaActual = BaseNitrogenedaEnum.Ninguno;
	
//	se toman en cuenta cuatro direcciones, por la recorrida de derecha a izquierda y de arriba hacia abajo contemplo todos los casos
	Map<DireccionesEnum, Integer> mapDireccionesRepetidos = new EnumMap<DireccionesEnum, Integer>(DireccionesEnum.class);


	public BaseNitrogenedaEnum getBaseNitrogenadaActual() {
		return baseNitrogenadaActual;
	}

	public void setBaseNitrogenadaActual(BaseNitrogenedaEnum baseNitrogenadaActual) {
		this.baseNitrogenadaActual = baseNitrogenadaActual;
	}

	public Map<DireccionesEnum, Integer> getMapDireccionesRepetidos() {
		return mapDireccionesRepetidos;
	}

	public void setMapDireccionesRepetidos(Map<DireccionesEnum, Integer> mapDireccionesRepetidos) {
		this.mapDireccionesRepetidos = mapDireccionesRepetidos;
	}
	
	public  Integer getMapCantDireccionesRepetidos(DireccionesEnum dir) {
		return mapDireccionesRepetidos.get(dir);
	}

	public void setMapCantDireccionesRepetidos(DireccionesEnum dir, Integer repiticiones) {
		this.mapDireccionesRepetidos.put(dir, repiticiones);
	}

}
