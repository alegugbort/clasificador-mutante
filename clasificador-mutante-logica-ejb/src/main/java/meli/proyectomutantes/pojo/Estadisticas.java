package meli.proyectomutantes.pojo;

import java.io.Serializable;

public class Estadisticas implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long cantidadMutantes; 
	private long cantidadHumanos;
	private double ratio;
	

	
	public Estadisticas(long cantidadMutantes, long cantidadHumanos, double ratio) {
		super();
		this.cantidadMutantes = cantidadMutantes;
		this.cantidadHumanos = cantidadHumanos;
		this.ratio = ratio;
	}
	
	public long getCantidadMutantes() {
		return cantidadMutantes;
	}
	public void setCantidadMutantes(long cantidadMutantes) {
		this.cantidadMutantes = cantidadMutantes;
	}
	public long getCantidadHumanos() {
		return cantidadHumanos;
	}
	public void setCantidadHumanos(long cantidadHumanos) {
		this.cantidadHumanos = cantidadHumanos;
	}
	public double getRatio() {
		return ratio;
	}
	public void setRatio(double ratio) {
		this.ratio = ratio;
	}
	
	
	
	
	
	
	

}
