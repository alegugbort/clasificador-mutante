package meli.proyectomutantes.modelo;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@SuppressWarnings("serial")
@Entity
public class DatoMutanteHumano implements Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;

	@Column
	private String adn;
	
	@Column
	private Boolean esMutante;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAdn() {
		return adn;
	}

	public void setAdn(String adn) {
		this.adn = adn;
	}

	public Boolean isEsMutante() {
		return esMutante;
	}

	public void setEsMutante(Boolean esMutante) {
		this.esMutante = esMutante;
	}

	@Override
	public String toString() {
		return "DatoMutanteHumano [id=" + id + ", adn=" + adn + ", esMutante=" + esMutante + "]";
	}

}
