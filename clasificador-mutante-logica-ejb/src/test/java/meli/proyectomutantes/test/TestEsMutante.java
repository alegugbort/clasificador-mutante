package meli.proyectomutantes.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import meli.proyectomutantes.cache.CacheEstadisticas;
import meli.proyectomutantes.colamensajes.ColaMensajes;
import meli.proyectomutantes.constantes.CantidadRepetidosDireccion;
import meli.proyectomutantes.dao.DatoMutanteHumanoDaoImpl;
import meli.proyectomutantes.excepciones.ExcepcionADNMalFormado;
import meli.proyectomutantes.manejadores.ManejadorMutanteHumano;
import meli.proyectomutantes.manejadores.ManejadorMutanteHumanoImpl;
import meli.proyectomutantes.modelo.DatoMutanteHumano;
import meli.proyectomutantes.pojo.ADN;
import meli.proyectomutantes.pojo.Estadisticas;

@RunWith(Arquillian.class)
public class TestEsMutante {

	private static Logger log = Logger.getLogger(TestEsMutante.class.getName());
	
	private final int tiempoEsperaPorGuardadoAsyncrono = 2000;
	
	
	@Deployment
	public static WebArchive createDeployment() {
//		Con este codigo creo un ejecutable que tiene todos los paquetes y la persitencia
		
		
//		Importo las dependencias maven
        File[] files = Maven.resolver()
                            .loadPomFromFile("pom.xml")
                            .importRuntimeDependencies()
                            .resolve()
                            .withTransitivity()
                            .asFile();
		
		return ShrinkWrap.create(WebArchive.class, "prueba.war").
				addPackage(CantidadRepetidosDireccion.class.getPackage()).
				addPackage(ExcepcionADNMalFormado.class.getPackage()).
				addPackage(ManejadorMutanteHumanoImpl.class.getPackage()).
				addPackage(DatoMutanteHumanoDaoImpl.class.getPackage()).
				addPackage(DatoMutanteHumano.class.getPackage()).
				addPackage(ColaMensajes.class.getPackage()).
				addPackage(CacheEstadisticas.class.getPackage()).
				addPackage(ADN.class.getPackage()).
				addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml").
				addAsWebInfResource("test-ds.xml", "test-ds.xml").
				addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml").
				addAsLibraries(files);
	}
	
	@Inject
	EntityManager em;
	
	@Inject
	ManejadorMutanteHumano manejadorHumanoMutante;
	
	
	private final String consultaMutanteHumanoDatoInsertado = "SELECT t FROM DatoMutanteHumano t where t.adn = :value1";
//	
// considero las direcciones como sur (arriba -> abajo), 
//	este (izquierda->derecha)
//	sureste (izquierda-arriba->derecha-abajo)
//	noreste (izquierda->abajo)->derecha-arriba)


	public void esperarAsync(int tiempo) {
		//Con este metodo le doy tiempo a que el metodo inserte en la base.
		try {
			Thread.sleep(tiempo);
		} catch (InterruptedException e) {
			log.info("Ocurrio un error al esperar por el metodo asyncrono");
		}
	}
	
	
	
	@Test
	@InSequence(1)  
	public void mutanteSurEste() {
		
		
		
		String [] cadena = {"ATGCGA",
		             		"CAGTCC",
		             		"TTATCT",
		             		"AGCAGG",
		             		"CCGAAA",
		             		"TCACTG"};
		
		boolean retorno = false;
		try {
			retorno = manejadorHumanoMutante.esMutante(cadena);
		} catch (ExcepcionADNMalFormado e) {

		}
		esperarAsync(tiempoEsperaPorGuardadoAsyncrono);
		log.info("Antes Consulta: " +String.join("-", cadena));
		DatoMutanteHumano datoMutante = (DatoMutanteHumano) em.createQuery(consultaMutanteHumanoDatoInsertado, DatoMutanteHumano.class).
				setParameter("value1", String.join("-", cadena)).getSingleResult();
		
		log.info("Despues Consulta: " +String.join("-", cadena));

		assertEquals(retorno, true);
		
		assertEquals(datoMutante.getAdn(), String.join("-", cadena));
		assertEquals(datoMutante.isEsMutante(), Boolean.valueOf(true));
		
	}
	
	
	@Test
	@InSequence(2)  
	public void mutanteEste() {
		
		
		String [] cadena = {"ATGCGA",
		             		"CTGTCC",
		             		"TTAACT",
		             		"AGCAGG",
		             		"CCAAAA",
		             		"TCACTG"};
		boolean retorno = false;
		try {
			retorno = manejadorHumanoMutante.esMutante(cadena);
		} catch (ExcepcionADNMalFormado e) {

		}
		
		
		esperarAsync(tiempoEsperaPorGuardadoAsyncrono);
		log.info("Antes Consulta: " +String.join("-", cadena));
		
		DatoMutanteHumano datoMutante = (DatoMutanteHumano) em.createQuery(consultaMutanteHumanoDatoInsertado, DatoMutanteHumano.class).
				setParameter("value1", String.join("-", cadena)).getSingleResult();
		log.info("Despues Consulta: " +String.join("-", cadena));
		
		assertEquals(retorno, true);
		
		assertEquals(datoMutante.getAdn(), String.join("-", cadena));
		assertEquals(datoMutante.isEsMutante(), Boolean.valueOf(true));
	}
	
	@Test
	@InSequence(3)  
	public void mutanteNorEste() {
		
		
		
		String [] cadena = {"ATGCGA",
		             		"CTGTCC",
		             		"TTACCT",
		             		"AGCAGG",
		             		"CCACAA",
		             		"TCACTG"};
		boolean retorno = false;
		try {
			retorno = manejadorHumanoMutante.esMutante(cadena);
		} catch (ExcepcionADNMalFormado e) {

		}
		esperarAsync(tiempoEsperaPorGuardadoAsyncrono);
		log.info("Antes Consulta: " +String.join("-", cadena));
		DatoMutanteHumano datoMutante = (DatoMutanteHumano) em.createQuery(consultaMutanteHumanoDatoInsertado, DatoMutanteHumano.class).
				setParameter("value1", String.join("-", cadena)).getSingleResult();
		log.info("Despues Consulta: " +String.join("-", cadena));
		assertEquals(retorno, true);
		
		assertEquals(datoMutante.getAdn(), String.join("-", cadena));
		assertEquals(datoMutante.isEsMutante(), Boolean.valueOf(true));
	}
	
	@Test
	@InSequence(4)  
	public void mutanteSur() {
		
		
		
		String [] cadena = {"ATGCGA",
		             		"CTGTCC",
		             		"TTACCT",
		             		"ATCAGG",
		             		"CTACAA",
		             		"TCACTG"};
		boolean retorno = false;
		try {
			retorno = manejadorHumanoMutante.esMutante(cadena);
		} catch (ExcepcionADNMalFormado e) {

		}
		esperarAsync(tiempoEsperaPorGuardadoAsyncrono);
		log.info("Antes Consulta: " +String.join("-", cadena));
		DatoMutanteHumano datoMutante = (DatoMutanteHumano) em.createQuery(consultaMutanteHumanoDatoInsertado, DatoMutanteHumano.class).
				setParameter("value1", String.join("-", cadena)).getSingleResult();
		log.info("Despues Consulta: " +String.join("-", cadena));
		
		assertEquals(retorno, true);
		
		assertEquals(datoMutante.getAdn(), String.join("-", cadena));
		assertEquals(datoMutante.isEsMutante(), Boolean.valueOf(true));
	}
	
	@Test
	@InSequence(5)  
	public void matrizMalFormada() {
		
		
		String [] cadena = {"ATGCGA",
		             		"CTGTCCAA",
		             		"TTACCT",
		             		"AGCAGG",
		             		"CCACAA"};
		boolean retornoMatrizMalFormadaExcepcion = false;
		boolean seEncontroElDato = true;
		
		try {
			 manejadorHumanoMutante.esMutante(cadena);
		} catch (ExcepcionADNMalFormado e) {
			retornoMatrizMalFormadaExcepcion = true;
		}

		esperarAsync(tiempoEsperaPorGuardadoAsyncrono);
		log.info("Antes Consulta: " +String.join("-", cadena));
		try {
			DatoMutanteHumano datoMutante = (DatoMutanteHumano) em.createQuery(consultaMutanteHumanoDatoInsertado, DatoMutanteHumano.class).
				setParameter("value1", String.join("-", cadena)).getSingleResult();
		}
		catch (NoResultException e) {
			seEncontroElDato = false;
		}
		log.info("Despues Consulta: " +String.join("-", cadena));
		
		assertEquals(retornoMatrizMalFormadaExcepcion, true);
		assertEquals(seEncontroElDato, false);
		
		
	}
	
	@Test
	@InSequence(6)  
	public void noMutante() {
		

		
		String [] cadena = {"ATGCGA",
		             		"CAGTCC",
		             		"TTACCT",
		             		"AGAGGG",
		             		"CCGAAA",
		             		"TCACTG"};
		boolean retorno = true;
		try {
			retorno = manejadorHumanoMutante.esMutante(cadena);
		} catch (ExcepcionADNMalFormado e) {

		}

		esperarAsync(tiempoEsperaPorGuardadoAsyncrono);
		log.info("Antes Consulta: " +String.join("-", cadena));
		DatoMutanteHumano datoMutante = (DatoMutanteHumano) em.createQuery(consultaMutanteHumanoDatoInsertado, DatoMutanteHumano.class).
				setParameter("value1", String.join("-", cadena)).getSingleResult();
		log.info("Despues Consulta: " +String.join("-", cadena));
		
		assertEquals(retorno, false);
		
		assertEquals(datoMutante.getAdn(), String.join("-", cadena));
		assertEquals(datoMutante.isEsMutante(), Boolean.valueOf(false));
	}
	
	@Test
	@InSequence(7)  
	public void noMutanteRepedito() {
		

		
		String [] cadena = {"ATGCGA",
		             		"CAGTCC",
		             		"TTACCT",
		             		"AGAGGG",
		             		"CCGAAA",
		             		"TCACTG"};
		boolean retorno = true;
		try {
			retorno = manejadorHumanoMutante.esMutante(cadena);
		} catch (ExcepcionADNMalFormado e) {

		}

		esperarAsync(tiempoEsperaPorGuardadoAsyncrono);
		log.info("Antes Consulta: " +String.join("-", cadena));
		List<DatoMutanteHumano> datosMutante = (List<DatoMutanteHumano>) em.createQuery(consultaMutanteHumanoDatoInsertado, DatoMutanteHumano.class).
				setParameter("value1", String.join("-", cadena)).getResultList();
		log.info("Despues Consulta: " +String.join("-", cadena));
		
		assertEquals(retorno, false);
		for (int i = 0; i < datosMutante.size(); i++)
		{
			assertEquals(datosMutante.get(i).getAdn(), String.join("-", cadena));
			assertEquals(datosMutante.get(i).isEsMutante(), Boolean.valueOf(false));
		}
	}


	
	@Test
	@InSequence(8)  
	public void consultaEstadisticas() {
		
		Estadisticas estadisticas = manejadorHumanoMutante.consultarEstaditicas();
		
		log.info("*****Estadisticas:*****");
		log.info("Cantidad mutantes:" + String.valueOf(estadisticas.getCantidadMutantes()));
		log.info("Cantidad humanos:" + String.valueOf(estadisticas.getCantidadHumanos()));
		log.info("Cantidad ratio:" + String.valueOf(estadisticas.getRatio()));
		log.info("*****Fin Estadisticas:*****");
		
		assertEquals(Double.valueOf(((double)estadisticas.getCantidadMutantes())/(estadisticas.getCantidadHumanos()+estadisticas.getCantidadMutantes())), Double.valueOf(estadisticas.getRatio()));
	
	}
	
	@Test
	@InSequence(9)  
	public void consultaEstadisticasCache() {
		
		Estadisticas estadisticas = manejadorHumanoMutante.consultarEstaditicas();
		
		log.info("*****Estadisticas:*****");
		log.info("Cantidad mutantes:" + String.valueOf(estadisticas.getCantidadMutantes()));
		log.info("Cantidad humanos:" + String.valueOf(estadisticas.getCantidadHumanos()));
		log.info("Cantidad ratio:" + String.valueOf(estadisticas.getRatio()));
		log.info("*****Fin Estadisticas:*****");
		
		assertEquals(Double.valueOf(((double)estadisticas.getCantidadMutantes())/(estadisticas.getCantidadHumanos()+estadisticas.getCantidadMutantes())), Double.valueOf(estadisticas.getRatio()));
	
	}
}
