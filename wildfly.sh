#Estas variables deben de ser modificadas para ejecutar localmente
export usuarioBD=${usuarioBD}
export passDB=${passDB}
export host=${host}
export rutaBD="jdbc:db2://${host}:50000/${nombreBD}:currentSchema=${schemaBD};"

cp -r ./clasificador-mutante-logica-ejb/target/site/jacoco ./wildfly/ejb-coverage
cp -r ./clasificador-mutante-rest-api-war/target/site/jacoco ./wildfly/war-coverage
cp -r ./clasificador-mutante-ear/target/clasificadorMutante.ear ./wildfly

docker-compose -f wildfly/docker-compose-wildfly.yml down
docker-compose -f wildfly/docker-compose-wildfly.yml up -d --build